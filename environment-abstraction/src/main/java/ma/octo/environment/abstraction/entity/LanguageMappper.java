package ma.octo.environment.abstraction.entity;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LanguageMappper implements RowMapper<Language> {
    @Override
    public Language mapRow(ResultSet rs, int rowNum) throws SQLException {
        Language language=new Language(
                rs.getString("id"),
                rs.getString("name"),
                rs.getString("author"),
                rs.getString("fileExtension")
        );
        return language;
    }
}
