package ma.octo.environment.abstraction.repository.impl;

import ma.octo.environment.abstraction.entity.Language;
import ma.octo.environment.abstraction.entity.LanguageMappper;
import ma.octo.environment.abstraction.repository.LanguageRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class LanguageRepositoryImpl implements LanguageRepository {


   private final JdbcTemplate jdbcTemplate;

  public LanguageRepositoryImpl(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }


  @Override
  public Optional<Language> findByExtension(final String extension) {
    String sql= "select * from language where fileExtension = ?";
    return Optional.ofNullable(jdbcTemplate.queryForObject(sql,new LanguageMappper(),new Object[]{extension}));
  }

  @Override
  public Optional<Language> findById(final String id) {
    String sql= "select * from language where id = ?";

    return Optional.ofNullable(jdbcTemplate.queryForObject(sql,new LanguageMappper(),new Object[]{id}));
  }

  @Override
  public List<Language> findAll() {
       String sql="select * from language";

    return jdbcTemplate.query(sql, new LanguageMappper());
  }


}
