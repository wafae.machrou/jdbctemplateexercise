package ma.octo.environment.abstraction.config;

import ma.octo.environment.abstraction.config.annotation.Development;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


import javax.sql.DataSource;

@Development
public class DevConfigProperties {



    @Autowired
    Environment environment;

    private final String URL = "app.db.url";
    private final String USER = "app.db.username";
    private final String DRIVER = "driver";
    private final String PASSWORD = "app.db.password";

    @Bean
    DataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setUrl(environment.getProperty(URL));
        driverManagerDataSource.setUsername(environment.getProperty(USER));
        driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
        driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
        return driverManagerDataSource;
    }
    @Bean
    JdbcTemplate jdbcTemplate(DataSource dataSource){
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        jdbcTemplate.execute("""

     DROP TABLE IF EXISTS language;
     CREATE TABLE language(id VARCHAR(255), name VARCHAR(255), author VARCHAR(255), fileExtension VARCHAR(255));

     INSERT INTO language(id, name, author, fileExtension)
     VALUES('java', 'Java', 'James Gosling', 'java'),
           ('cpp', 'C++', 'Bjarne Stroustrup', 'cpp'),
           ('csharp', 'C#', 'Andres Hejlsberg', 'cs'),
           ('perl', 'Perl', 'Larry Wall', 'pl'),
           ('haskel', 'Haskell', 'Simon Peyton', 'hs'),
           ('lua', 'Lua', 'Luiz Henrique', 'lua'),
           ('python', 'Python', 'Guido van Rossum', 'py');

    """);
        return jdbcTemplate;
    }



}
